hsOsaES6
========
[![npm version](https://badge.fury.io/js/hsosaes6.svg)](https://badge.fury.io/js/hsosaes6)
--[![Build status](https://ci.appveyor.com/api/projects/status/sw91uymqktwajoxp?svg=true)](https://ci.appveyor.com/project/HelpfulScripts/hsosaes6)
[![Built with Grunt](https://cdn.gruntjs.com/builtwith.svg)](https://gruntjs.com/) 
[![NPM License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://www.npmjs.com/package/hsosaes6) 

Helpful Scripts ES6 interface to various OSX functions using the OSA Architecture.

## Installation
`npm i hsosaes6`

See [docs](https://helpfulscripts.github.io/hsOsaES6/indexGH.html#!/api/hsOsaES6/0) for details
